<?php

namespace TBaronnat\MailerBundle\Listener;

use TBaronnat\MailerBundle\Event\SendMailEvent;
use TBaronnat\MailerBundle\MailerInterface;

class MailerListener
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function onSendMailEvent(SendMailEvent $event): void
    {
        $this->mailer->send($event->getMailType(), $event->getParameters(), $event->getAttachments());
    }
}
