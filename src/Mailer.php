<?php

namespace TBaronnat\MailerBundle;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Mailer as SfMailer;

class Mailer implements MailerInterface
{
    protected ?SfMailer $mailer;
    protected ?MailerConfig $mailConfig = null;

    public function __construct(SfMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function setConfig(array $config)
    {
        $this->mailConfig = new MailerConfig($config);
    }

    public function send(string $mailId, array $parameters = [], array $attachments = []): int
    {
        try {
            $this->mailConfig->load($mailId);
            $this->mailConfig->replaceParameters($parameters);

            $email = (new TemplatedEmail())
                ->from($this->mailConfig->getFrom())
                ->to($this->mailConfig->getTo())
                ->subject($this->mailConfig->getSubject())
                ->htmlTemplate($this->mailConfig->getTemplate())
                ->context($parameters)
            ;

            $this->addAttachments($email, $attachments);

            $this->mailer->send($email);

            return 1;
        } catch (\Exception $e) {
        }

        return 0;
    }

    private function addAttachments(TemplatedEmail &$email, array $attachments = [])
    {
        foreach ($attachments as $file => $name) {
            $email->attach($file, $name);
        }
    }
}
