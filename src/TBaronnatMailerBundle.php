<?php

namespace TBaronnat\MailerBundle;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use TBaronnat\MailerBundle\DependencyInjection\MailerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TBaronnatMailerBundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        if ($this->extension == null) {
            $this->extension = new MailerExtension();
        }

        return $this->extension;
    }
}