<?php

namespace TBaronnat\MailerBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class SendMailEvent extends Event
{
    public const EVENT_NAME = 'event.send_mail';
    private string $mailType;
    private ?array $parameters;
    private ?array $attachments;
    private string $localeCode;

    public function __construct(string $mailType, string $localeCode, ?array $parameters = [], ?array $attachments = [])
    {
        $this->mailType = $mailType;
        $this->parameters = $parameters;
        $this->localeCode = $localeCode;
        $this->attachments = $attachments;
    }

    public function getLocaleCode(): string
    {
        return $this->localeCode;
    }

    public function getMailType(): string
    {
        return $this->mailType;
    }

    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    public function getAttachments(): ?array
    {
        return $this->attachments;
    }
}
