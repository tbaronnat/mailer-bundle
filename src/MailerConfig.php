<?php

namespace TBaronnat\MailerBundle;

use Symfony\Component\Mime\Address;

class MailerConfig
{
    protected ?array $fullConfig = [];
    protected ?array $loadedConfig = null;

    public function __construct(?array $fullConfig = [])
    {
        $this->fullConfig = $fullConfig;
    }

    public function load(string $mailId): self
    {
        if (!isset($this->fullConfig[$mailId])) {
            throw new \Exception(sprintf('Missing mailId %s configuration', $mailId));
        }

        $this->loadedConfig = $this->fullConfig[$mailId];

        return $this;
    }

    public function replaceParameters(array $parameters): ?array
    {
        $this->checkIsLoaded();

        $parametersSearch = [];
        foreach ($parameters as $key => $value) {
            if (!is_array($value) && !is_object($value)) {
                $parametersSearch[$key] = sprintf('§%s§', $key);
            } else {
                unset($parameters[$key]);
            }
        }

        array_walk($this->loadedConfig, function (&$value) use ($parametersSearch, $parameters) {
            $value = str_replace($parametersSearch, $parameters, $value);
        });

        return $this->loadedConfig;
    }

    public function getFrom(): ?Address
    {
        return new Address($this->getData('from'), $this->getData('fromName') ?? null);
    }

    public function getTo(): string
    {
        $to = $this->getData('to');

        if (!is_array($to)) {
            return $to;
        }

        return implode(', ', $to);
    }

    public function getSubject(): string
    {
        return $this->getData('subject');
    }

    public function getTemplate(): string
    {
        return $this->getData('template');
    }

    private function checkIsLoaded()
    {
        if (null === $this->loadedConfig) {
            throw new \Exception('MailConfig was not loaded');
        }
    }

    private function getData(string $key)
    {
        $this->checkIsLoaded();

        if (!isset($this->loadedConfig[$key])) {
            throw new \Exception(sprintf('Missing key %s into loaded configuration', $key));
        }

        return $this->loadedConfig[$key];
    }
}
