Exemple of use : 

    $sendMailEvent = (new SendMailEvent(
           'registration_confirmation',
           [
                'subject' => "Confirmation d'inscription",
                'toEmail' => 'client@gmail.com'
           ])
       );
       $this->dispatcher->dispatch($sendMailEvent, SendMailEvent::EVENT_NAME);
       
       
In tbaronnat_mailer.yaml:

    tbaronnat_mailer:
      config:
        registration_confirmation:
          subject: '§subject§'
          to: ['§toEmail§']
          from: 'noreply@theo-baronnat.fr'
          fromName: 'Theo B'
          template: 'mails/registration.html.twig'
