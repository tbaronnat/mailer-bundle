<?php

namespace TBaronnat\MailerBundle;

interface MailerInterface
{
    public function setConfig(array $config);

    public function send(string $mailId, array $parameters = [], array $attachments = []): int;
}
